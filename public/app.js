(function() {

  return {

    defaultState: 'loading',

    requests: {

      fetchTickets: function(user_id) {
        return {
          url: this.settings.ft_api_url + '/zendesk/zendesk_new.php',
          type: 'POST',
          dataType: 'json',
          data: {
            'action': 'ticket',
            'uid': user_id
          }
        };
      },

      fetchLabels: function(user_id) {
        return {
          url: this.settings.ft_api_url + '/zendesk/zendesk_new.php',
          type: 'POST',
          dataType: 'json',
          data: {
            action: 'labels',
            uid: user_id
          }
        };
      },

      fetchShopifyOrder: function(order_id) {
        return {
          url: this.settings.ft_api_url + '/zendesk/shopify_new.php',
          type: 'POST',
          dataType: 'json',
          data: {
            oid: order_id
          }
        };
      },

      createLabel: function(order) {
        return {
          url: this.settings.ft_api_url + '/zendesk/zendesk_new.php',
          type: 'POST',
          dataType: 'json',
          data: {
            action: 'create_label',
            name: order.shipping_address.name,
            addr1: order.shipping_address.address1,
            addr2: order.shipping_address.address2,
            province: order.shipping_address.province,
            company: order.shipping_address.company,
            province_code: order.shipping_address.province_code,
            zip: order.shipping_address.zip,
            phone: order.shipping_address.phone,
            uid: this.ticket().requester().id(),
            oid: order.id,
            onum: order.order_number,
            ticket: this.ticket().id()
          }
        };
      }

    },

    events: {

      // Start app, fetch ticket history of user
      'app.activated': 'init',

      'fetchTickets.always': function(data) {
        this.store('zendesk.tickets', { tickets: data.tickets });
        this.renderTickets({tickets: data.tickets, settings: this.settings});
      },

      'fetchLabels.always': function(data) {
        this.store('zendesk.labels', { labels: data.labels });
        this.renderLabels({ labels: data.labels, settings: this.settings});
      },

      'fetchShopifyOrder.always': function(data) {
        this.store('shopify.order', { order: data.order });
        this.renderShopify({ order: data.order, settings: this.settings});
      },

      'createLabel.always': function(data) {

        if(data.error) {
          services.notify(data.error);
        }
        else {
          this.renderLabels();
        }
      },

      'click #search': function(e) {
        e.preventDefault();
        var order_number = this.$('#order_number').val();
        if(order_number !== undefined)
        {
          this.ajax('fetchShopifyOrder', order_number);
        }
      },

      'click #new': function(e) {
        e.preventDefault();
        this.renderCustomer(this.store('shopify.order'));
      },

      'click #user-add': function(e) {
        e.preventDefault();
        this.store('shopify.order', this.createCustomer());
        this.renderShopify(this.store('shopify.order'));
      },

      'click #user-cancel': function(e) {
        e.preventDefault();
        this.renderShopify(this.store('shopify.order'));
      },

      'click #label': function(e) {
        e.preventDefault();
        this.ajax('createLabel', this.store('shopify.order').order);
      },

      'click #tickets-btn': function(e) {
        e.preventDefault();
        this.renderTickets(this.store('zendesk.tickets'));
      },

      'click #labels-btn': function(e) {
        e.preventDefault();
        this.renderLabels();
      }, 

      'click #shopify-btn': function(e) {
        e.preventDefault();
        this.renderShopify(this.store('shopify.order'));
      }

    },

    init: function(data) {
      this.ajax('fetchTickets', this.ticket().requester().id());
    },

    renderTickets: function(data) {
      // ember.js loads application settings as a param on first-run
      if(data.firstLoad) {
        this.ajax('fetchTickets', this.ticket().requester().id());
      }
      else {
        data = data || this.ajax('fetchTickets', this.ticket().requester().id());
        this.switchTo('tickets', data);
      }
    },

    renderLabels: function(data) {
      data = data || this.ajax('fetchLabels', this.ticket().requester().id());
      this.switchTo('labels', data);
    },

    renderShopify: function(data) {
      this.switchTo('shopify', data);
    },

    renderCustomer: function(data) {
      data = data || {};
      this.switchTo('customer', data);
      this.$('#new-customer-state').val(data.order.shipping_address.province_code || {});
    },

    createCustomer: function() {
      return {
        order: {
          name: null,
          id: null,
          shipping_address: {
            name :  this.$('#new-customer-name').val(),
            phone : this.$('#new-customer-phone').val(),
            address1 : this.$('#new-customer-addr1').val(),
            address2 : this.$('#new-customer-addr2').val(),
            province : this.$('#new-customer-city').val(),
            province_code : this.$('#new-customer-state').val(),
            zip : this.$('#new-customer-zip').val(),
            company: ''
          }
        }
      };
    }

  };

}());